from __future__ import print_function
from brain import *
import json
import socket
import sys


class NoobBot(object):
    ### static variable for switching lanes
    left = "Left"
    right = "Right"

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.accelerate = True
        self.throttleValue = 1.0
        self.turboAvailable = False
        self.prev_trackindex = 0

        self.tracks = [] # keeps Track object from game_init packet

        ### a file for storing all the information for the car positions (tab delimited)
        self.car_position_f = open("car_position.log", "w+")
        labels = ['GameTick','CarName','Color','Angle','TrackPiece','PosInPiece']
        print ('\t'.join(map(str,labels)),file=self.car_position_f)

        ### a file for storing all the information for the car velocity
        self.car_velocity_f = open("car_velocity.log","w+")

    def msg(self, msg_type, data, gameTick=-1):
        if gameTick < 0:
            self.send(json.dumps({"msgType": msg_type, "data": data}))
        else:
            self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": gameTick}))

    def send(self, msg):
        self.socket.sendall(msg + "\n") # applied fix: send -> sendall

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def create_race(self):
        return self.msg("createRace",{"botId":{"name": self.name,"key": self.key},"trackName": "hockenheim","password": "ftm","carCount": 1})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    ### narae: sends switch message
    def switch_lane(self,msg):
        if msg == NoobBot.left or msg == NoobBot.right:
            print("switching lane to ",msg)
            self.msg("switchLane",msg)

    def ping(self):
        self.msg("ping", {})

    ### narae: sends turbo msg
    def turbo(self):
        print("Pow pow pow pow pow")
        self.msg("turbo", "Pow pow pow pow pow")

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    ### narae: on receiving your car packet
    def on_yourcar(self, data):
        print("name: ",data['name'])
        print("color: ",data['color'])

    ### narae: on receiving game init packet
    def on_game_init(self, data):
        print("Game init")
        print("Track: ",data['race']['track']['name'])
        self.log_track_pieces_info(data['race']['track']['pieces'])
        print("cars: ", data['race']['cars'])
        # print("raceSession: ", data['race']['raceSession'])

        self.last_dash(self.tracks)
        # keeps record of last lap index
        self.last_lap = data['race']['raceSession']['laps'] - 1
        self.laneSwitcher = LaneSwitcher(self.tracks)

    ### narae: stores Track objects in "tracks" and prints out track pieces on a file
    def log_track_pieces_info(self, data):
        f = open("track_info.log","w+")
        labels = ['index','bend','length','angle','radius','switch']
        print ('\t'.join(map(str,labels)),file=f) # printing header
        for ind, piece in enumerate(data):
            #print("ind: ", ind, ", piece: ", piece) #jiwon, TODO: erase
            self.tracks.append(Track(piece))
            print(self.tracks[-1],file=f) # printing out the track pieces

    def on_turbo_available(self, data):
        print("Turbo Available")
        self.turboAvailable = True

    def on_turbo_end(self,data):
        print("Turbo Ended")
        self.turboAvailable = False

    ### narae: finds out when to start using turbo for last minute dash!
    def last_dash(self,tracks):
        self.dash_start_track_index = 0
        for ind, t in enumerate(tracks):
            if t.bend:
                self.dash_start_track_index = ind + 1

        print("dashing track piece index will be ",self.dash_start_track_index)

    def on_game_start(self, data):
        print("Race started")
        self.ping()
        self.throttleValue = 0.6 # 0.7
        self.switch_lane(self.laneSwitcher.next_switch())

    ### narae: on receiving car positions packet
    def on_car_positions(self, data, tick=-1):
        for car in data:
            #jiwon 
            if car['id']['name'] == self.name:
                trackindex = int(car['piecePosition']['pieceIndex'])

                '''
                strategic move 1: use smart switching
                '''
                if self.prev_trackindex < trackindex and self.tracks[trackindex].switch:
                    if not trackindex == self.laneSwitcher.switch_tracks[-1]:
                        self.switch_lane(self.laneSwitcher.next_switch())
                    self.prev_trackindex = trackindex

                '''
                strategic move 2: using turbo ONCE(hopefully once) at the end of the race on straight track pieces
                '''
                # if self.turboAvailable and trackindex == self.dash_start_track_index and int(car['piecePosition']['inPieceDistance']) < self.throttleValue*10:
                #     self.turbo()

                '''
                strategic move 3:
                    dashes like crazy for the very last lap
                    (cuz we don't care if the car goes over board later.. anyways)
                '''
                if self.turboAvailable and trackindex >= self.dash_start_track_index and car['piecePosition']['lap'] == self.last_lap:
                    self.turbo() # turbo like crazily in the end

                self.throttle(self.throttleValue)
                '''
                strategic move 4: varying throttle for being tracks (jiwon)
                '''
                # if self.tracks[trackindex].bend == True:
                #     # print(tick, "\t", trackindex, "\t", car['piecePosition']['inPieceDistance'], "\t", car['angle'], "\t", self.throttleValue)
                #     if self.accelerate == True:
                #         if car['angle'] > self.tracks[trackindex].data['angle']/2:
                #             self.accelerate = False
                #             self.throttleValue = 0.3
                #             self.throttle(0.3)
                #         else:
                #             self.throttleValue = 0.7
                #             self.throttle(0.7)
                #     else: 
                #         if car['angle'] < self.tracks[trackindex].data['angle']/4:
                #             self.accelerate = True
                #             self.throttleValue = 0.7
                #             self.throttle(0.7)
                #         else:
                #             self.throttleValue = 0.3
                #             self.throttle(0.3)
                        
                # else:
                #     self.throttleValue = 0.7
                #     self.throttle(0.7)
                    #if self.tracks[(trackindex+1) % len(self.tracks)].bend == True:
                    #    self.throttle(0.0)
                    #else:
                    #    self.throttle(1.0)
                #print("piece index: ", car['piecePosition']['pieceIndex'])
                #print("piece info: ", self.tracks[car['piecePosition']['pieceIndex']])
                #print("in-piece distance: ", car['piecePosition']['inPieceDistance']) 
                #print("in-piece distance: ", car['piecePosition']['inPieceDistance'], "\tdrift angle: ", car['angle'])
            #jiwon end
            self.log_car_positions_info(car,tick)

    def log_car_velocity(self, data, gameTick=-1):
        ### a record for keeping track of current velocity (dist/tick)
        if self.old_car_pos is none:
            self.old_car_pos = data
            print(0, file=self.car_velocity_f)
            return

        # if ['piecePosition']['pieceIndex']['pieceIndex'] > self.old_car_pos['piecePosition']['pieceIndex']:
        #     # entered next track piece
        #     ##
        # else:
        #     velocity = (data['piecePosition']['inPieceDistance'] - self.old_car_pos['piecePosition']['inPieceDistance'])/1.0
        #     print(velocity, file=self.car_velocity_f)

    ### narae: helper function that prints car position packet onto the file
    def log_car_positions_info(self, data, gameTick=-1):
        printInfo = [gameTick,data['id']['name'],data['id']['color'],data['angle'],data['piecePosition']['pieceIndex'],data['piecePosition']['inPieceDistance']]
        print('\t'.join(map(str,printInfo)),file=self.car_position_f)

    def on_crash(self, data):
        print( "%s (%s) crashed" % (data['name'], data['color']))
        self.ping()

    ### narae: receives lapFinished packet
    def on_lap_finished(self,data):
        if data['car']['name'] == self.name:
            print("Lap ",data['lapTime']['lap']," finished in ",data['lapTime']['millis'])
            self.prev_trackindex = 0
            self.laneSwitcher.next_switch_index = 0
            self.switch_lane(self.laneSwitcher.next_switch())   

    def on_game_end(self, data):
        print("Race ended")
        f = open("gameend.log", "w+")
        print(data, file=f)
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_yourcar,
            'turboAvailable': self.on_turbo_available, # added
            'turboEnd': self.on_turbo_end,          # added
            'gameInit': self.on_game_init,          # added
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,  # added
            'lapFinished': self.on_lap_finished,    # added
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if 'gameTick' in msg and msg_type == 'carPositions':
                    msg_map[msg_type](data,msg['gameTick'])
                else:
                    msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
