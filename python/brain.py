from __future__ import print_function
import math
import numpy as np
from numpy.linalg import norm

class Smarter(object):

    def __init__(self):
        self.tracks = []

    '''
    stores the track info
    '''
    def storeTrackInfo(self,pieces):
        f = open("track_info.log","w+")
        labels = ['index','bend','length','Angle','radius','switch']
        print ('\t'.join(map(str,labels)),file=self.car_position_f)
        for ind, p in enumerate(pieces):
            bend = 'angle' in p
            self.tracks.append(Track(p,bend))
            print(self.tracks[-1],file=f) # printing out the track pieces 

    def on_bend(self, trackindex):
        # if self.tracks[trackindex].bend:
        #     angle, radius = self.tracks[trackindex].getBendData()
        #     rangle = math.radians(angle) # degree to radian
        #     source = np.array([math.copysign(1.0,angle)*radius,0.0]) # getting vector cor current car with the center, [0,0]
        #     target = np.array([source + radius*math.sin(rangle), source + radius*math.cos(rangle)])
        #     return norm(np.subtract(target,source)/norm(np.subtract(target,source)))  # I don't know what I'm doing...
        # else:
        #     return 0.7
        print("bend!")
        return 0.6

class Track(object):
    def __init__(self, data):
        self.data = data
        self.bend = False
        self.switch = False
        if 'angle' in data:
            self.bend = True

        if 'switch' in data:
            self.switch = True
        self.printarray = [self.bend] # in the following order: 'bend','length','Angle','radius','switch'
        if self.bend:
            self.printarray = self.printarray + ['',self.data['angle'], self.data['radius'], self.switch]
        else:
            self.printarray = self.printarray + [self.data['length'],'','',self.switch]


    def __str__(self):
        if self.bend == True:
            return "Angle = " + str(self.data['angle']) + ", Radius = " + str(self.data['radius'])
        else:
            return "Length = " + str(self.data['length'])
        
    def __repr__(self):
        return '\t'.join(map(str,self.printarray))

    def getData(self):
        if self.bend:
            return self.data['angle'], self.data['radius']
        else:
            return self.data['length']


def getRadian(degree):
    return math.radians(degree) # degree to radian

# note that the radius is distance from the circle center and the track center
# for now, we are assuming that the track only have two lanes, but if it gets bigger, we should change it!
def getBendingTrackLength(degree,radius,carwidth,lane):
    if lane == 0:
        return math.radians(degree) * (radius + math.copysign(1.0,angle)*carwidth/2)
    elif lane == 1:
        return math.radians(degree) * (radius - math.copysign(1.0,angle)*carwidth/2)

'''
So the theory behind this is that if upcoming tracks bend towards one side more than the other,
it's beneficial to switch lane to the bending side to reduce the overall travel distance.
'''
class LaneSwitcher(object):
    def __init__(self,tracks):
        left = "Left"
        right = "Right"

        self.tracks = tracks
        self.switch_tracks = []
        self.to_which_sides = []
        self.next_switch_index = 0
        for i,t in enumerate(tracks):
            if 'switch' in t.data and t.data['switch']:
                self.switch_tracks.append(i)

        last_switch_track_index = 0
        for i in range(0,len(self.switch_tracks)-1):
            self.to_which_sides.append(self.switch_to_which_side(self.switch_tracks[i],self.switch_tracks[i+1]))

        # take care of last piece
        self.to_which_sides.append(self.switch_to_which_side(self.switch_tracks[i],len(self.tracks)))
        print ("Switching track indexes: ",self.switch_tracks)
        print ("Switching sides: ",self.to_which_sides)

    '''
    returns "Left" or "Right" determining which side we should switch lane to for the upcoming tracks
    '''
    def switch_to_which_side(self,fromi,toi):
        rightbend_count = 0
        leftbend_count = 0
        for i,t in enumerate(self.tracks[fromi:toi]):
            if t.bend:
                if math.copysign(1.0,t.data['angle']) > 0:
                    rightbend_count += 1
                elif math.copysign(1.0,t.data['angle']) < 0:
                    leftbend_count += 1

        if rightbend_count > leftbend_count:
            return "Right"
        elif leftbend_count > rightbend_count:
            return "Left"
        else:
            return "Stay" # dummy message to denote not to switch lane!

    def next_switch(self):
        thistime = self.to_which_sides[self.next_switch_index]
        self.next_switch_index += 1
        self.next_switch_index %= len(self.to_which_sides)
        return thistime

    def next_switching_lane(self):
        return self.switch_tracks[self.next_switch_index]